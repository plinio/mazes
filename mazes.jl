using OffsetArrays

const version = "0.1.0"

const MIN_HEIGHT = 4
const MAX_HEIGHT = 20
const MIN_WIDTH = 4
const MAX_WIDTH = 30

## Defaults
H = 12
W = 20
visibility = 1

const help_str = """
mazes.jl $version
Plínio Valério <plinio.valerio@pm.me>
A simple maze game for the terminal

OPTIONS:
    -s, --size <HEIGHT> <WIDTH>    Maze dimensions (default: $H x $W)
    -V, --visibility <VALUE>       Maze visibility [possible values: 0, 1, 2]
    -v, --version                  Print program version
    -h, --help                     Print help information
"""

while !isempty(ARGS)
    global H, W, visibility
    arg = popfirst!(ARGS)
    if arg == "-h" || arg == "--help"
        print(help_str)
        exit()
    elseif arg == "-v" || arg == "--version"
        println("mazes.jl $version")
        exit()
    elseif arg == "-s" || arg == "--size"
        try
            H = parse(Int, popfirst!(ARGS))
            W = parse(Int, popfirst!(ARGS))
        catch
            error("parameter '$arg' expects two integers")
        end
        if !(MIN_HEIGHT <= H <= MAX_HEIGHT)
            error("height must be between $MIN_HEIGHT and $MAX_HEIGHT")
        end
        if !(MIN_WIDTH <= W <= MAX_WIDTH)
            error("width must be between $MIN_WIDTH and $MAX_WIDTH")
        end
    elseif arg == "-V" || arg == "--visibility"
        try
            visibility = parse(Int, popfirst!(ARGS))
            @assert 0 <= visibility <= 2
        catch
            error("parameter '$arg' expects an integer between 0 and 2")
        end
    else
        error("parameter '$arg' not recognized, see '--help' for valid options")
    end
end

## ====================================================== ##

hwalls = OffsetArray(ones(Bool, H+1, W),  0:H,      0:(W-1))
vwalls = OffsetArray(ones(Bool, H, W+1),  0:(H-1),  0:W)

for i in 1:(H-1);   hwalls[i, 0] = false;   end
up = true
i = H-1
j = 1
while j < W
    global i, j, up
    vwalls[i,j] = false
    if up
        i -= 1
        while i >= 0
            (rand() < 0.5) ? (hwalls[i+1,j] = false) : (vwalls[i,j] = false)
            i -= 1
        end
        i += 1  ## returns to board
    else
        i += 1
        while i < H
            (rand() < 0.5) ? (hwalls[i,j] = false) : (vwalls[i,j] = false)
            i += 1
        end
        i -= 1  ## returns to board
    end
    j += 1
    up = !up
end

goal = CartesianIndex(rand(0:(H-1)),  rand(0:(W-1)))
you  = CartesianIndex(rand(0:(H-1)),  rand(0:(W-1)))


const interior_cell_width = 3
const k = interior_cell_width + 1
ascii_idx(idx) = CartesianIndex(Tuple(idx) .* (2, k) .+ (1, k÷2))

board = OffsetArray(fill(' ', 2H+1, k*W+1),  0:2H,  0:(k*W))

function update_board!()
    (visibility == 2) && return
    for delta in (
            CartesianIndex( 1, 0),
            CartesianIndex( 0, 1),
            CartesianIndex(-1, 0),
            CartesianIndex( 0,-1)
        )
        nb = you + delta
        (i, j) = Tuple(nb)
        (0 <= i < H) || continue
        (0 <= j < W) || continue

        if hwalls[nb]
            board[2i, (k*j+1):(k*j+interior_cell_width)] .= '-'
            board[2i, k*j] = '+'
            board[2i, k*(j+1)] = '+'
        end
        if hwalls[nb + CartesianIndex(1,0)]
            board[2*(i+1), (k*j+1):(k*j+interior_cell_width)] .= '-'
            board[2*(i+1), k*j] = '+'
            board[2*(i+1), k*(j+1)] = '+'
        end
        if vwalls[nb]
            board[2i+1, k*j] = '|'
            board[2i,   k*j] = '+'
            board[2i+2, k*j] = '+'
        end
        if vwalls[nb + CartesianIndex(0,1)]
            board[2i+1, k*(j+1)] = '|'
            board[2i,   k*(j+1)] = '+'
            board[2i+2, k*(j+1)] = '+'
        end
    end
end

if visibility == 2  ## full visibility
    for idx in CartesianIndices(hwalls)
        local i, j
        (i, j) = Tuple(idx)
        board[2i, k*j] = '+'
        hwalls[idx] || continue
        board[2i, (k*j+1):(k*j+interior_cell_width)] .= '-';
    end
    for i in 0:H;   board[2i, k*W] = '+';   end
    for idx in CartesianIndices(vwalls)
        local i, j
        (i, j) = Tuple(idx)
        vwalls[idx] || continue
        board[2i+1, k*j] = '|'
    end
elseif visibility == 1
    error("visibility 1 not yet implemented, sorry")
elseif visibility == 0  ## only sees imediate neighborhood
    nothing
end

## ====================================================== ##

function print_maze()
    update_board!()
    board_s = ""
    goal_board = ascii_idx(goal)
    you_board  = ascii_idx(you)
    board[goal_board] = '$'
    board[ you_board] = '\u263b'
	for i in 0:2H
		for j in 0:(k*W);   board_s *= board[i,j];   end
		board_s *= '\n'
	end
    board[goal_board] = ' '
    board[ you_board] = ' '
    print(board_s)
end

while true
    global you
    print('\n'^40)
    print_maze()
    (you == goal) && break

    action = read(stdin, Char)
    if     action == 'w'  ## up
        (you[1] > 0) || continue
        hwalls[you] && continue
        you += CartesianIndex(-1, 0)
    elseif action == 'a'  ## left
        (you[2] > 0) || continue
        vwalls[you] && continue
        you += CartesianIndex(0, -1)
    elseif action == 's'  ## down
        (you[1] < H-1) || continue
        next = you + CartesianIndex(1, 0)
        hwalls[next] && continue
        you = next
    elseif action == 'd'  ## right
        (you[2] < W-1) || continue
        next = you + CartesianIndex(0, 1)
        vwalls[next] && continue
        you = next
    end
end

println()
println("YOU WON!")
exit()
